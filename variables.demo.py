"""
1- Bir müşterinin aşağıdaki bilgileri için değişken oluşturunuz.


Müşteri adı
Müşteri soyadı
Müşteri ad + soyad
Müşteri cinsiyet
Müşteri tc kimlik
Müşteri doğum yılı
Müşteri adres bilgisi
Müşteri yaşı

"""
musteriAdi = "Metin"

musteriSoyad = " YAZICI "

musteriAdSoyad = musteriAdi + " " + musteriSoyad

musteriCins = True # Erkek

musteriTc = " 224403003476 "

musteriDogum = 2001

musteriAdres = " Bursa İnegöl"

musteriYas = 2023 - musteriDogum


"""
2- Aşağıdaki siparişlerin toplam fiyatlarını hesaplayınız.

Sipariş 1 => 110    TL
Sipariş 2 => 1100.5 TL
Sipariş 3 => 356.95 TL


"""
sayi1 = 110

sayi2 = 1100.5

sayi3 = 356.95

toplamSayi = sayi1+sayi2+sayi3

print("ToplamSayi: ", toplamSayi  )